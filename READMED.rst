# GESTOR DE PRODUCTOS

## Description

Pasos para la instalación y la ejecución del gestor de productos.

## Requirements

### Build

Xcode 9.2, macOS 12.0 ó posteriro

## Instalación

El código se encuentra escrito en Swift 4, este código contiene unas librerías que están instaladas con cocoapods, en caso de querer reinstalar las librerías debe ser desde una consola.

CocoaPods:
* https://guides.cocoapods.org/using/getting-started.html

Instalar pods

* Desde la raíz del proyecto debe ejecutar `pod install`, para poder hacer esto, debe estar instalado CocoaPods en la MAC.

* https://guides.cocoapods.org/using/getting-started.html

## Estructura del código

* Modelo, vista, controlador [MVC]: El código esta dividido en 3 carpetas [MVC] esta estructura de carpetas es lógica.

* En la carpeta de `Controllers` se encuentran otra serie de carpetas donde se encuentran alojados los controladores de cada vista de de la aplicación.

* En la carpeta de `Views` se encuentran alojados los archivos de diseño, vistas e imágenes.

* En la carpeta de `Models` se encuentra un modelo que es  `Product`.


## Librerías

Libreria | Versión
------------ | -------------
TextFieldEffects | 1.4
NVActivityIndicatorView | 4.0.1
IQKeyboardManagerSwift | 5.0.6
Siesta/Alamofire | 1.0

## Licencia

Leonardo Flórez López
