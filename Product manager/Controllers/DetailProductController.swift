//
//  DetailProductController.swift
//  Product manager
//
//  Created by Leonardo Florez Lopez on 10/17/18.
//  Copyright © 2018 Leonardo Florez Lopez. All rights reserved.
//

import UIKit
import MapKit

class DetailProductController: UIViewController, MKMapViewDelegate {

    @IBOutlet var map: MKMapView!
    
    var product:Product!
    @IBOutlet var imageProduct: UIImageView!
    @IBOutlet var nameProduct: UILabel!
    @IBOutlet var priceProduct: UILabel!
    @IBOutlet var expirationDateProduct: UILabel!
    @IBOutlet var createdDateProduct: UILabel!
    
    // Se crean variables para el campo de vision del mapa
    let latDelta:CLLocationDegrees = 0.01
    let longDelta:CLLocationDegrees = 0.01
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Detalle"
        
        self.nameProduct.text = self.product.nameProduct
        self.priceProduct.text = self.product.priceProduct
        self.expirationDateProduct.text = "Fecha de caducidad: \(String(describing: self.product.expirationDateProduct!))"
        self.createdDateProduct.text = "Fecha de creación: \(String(describing: self.product.createdDateProduct!))"
        
        map.delegate = self
        
        if !self.product.latitud.isEmpty{
            let span:MKCoordinateSpan = MKCoordinateSpan(latitudeDelta: latDelta, longitudeDelta: longDelta)
            let location:CLLocationCoordinate2D = CLLocationCoordinate2DMake(CLLocationDegrees(self.product.latitud)!, CLLocationDegrees(product.longitud)!)
            let region:MKCoordinateRegion = MKCoordinateRegion(center: location, span: span)
            
            map.setRegion(region, animated: true)
            
            // Se crea un marcador con la latitud y longitud dada
            let markerProduct = MKPointAnnotation()
            markerProduct.coordinate = location
            markerProduct.title = self.product.nameProduct
            markerProduct.subtitle = self.product.priceProduct
            
            map.addAnnotation(markerProduct)
        }
    }
}
