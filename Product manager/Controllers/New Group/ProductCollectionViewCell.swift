//
//  ProductCollectionViewCell.swift
//  Product manager
//
//  Created by Leonardo Florez Lopez on 10/17/18.
//  Copyright © 2018 Leonardo Florez Lopez. All rights reserved.
//

import Foundation
import UIKit

class ProductCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var imageProduct: UIImageView!
    @IBOutlet var nameproduct: UILabel!
    @IBOutlet var priceProduct: UILabel!
}
