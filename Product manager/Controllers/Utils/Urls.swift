//
//  Urls.swift
//  Product manager
//
//  Created by Leonardo Florez Lopez on 10/19/18.
//  Copyright © 2018 Leonardo Florez Lopez. All rights reserved.
//

import Siesta

let APIService = API()

class API: Service {
    
    init() {

        //URL BASE
        //super.init(baseURL: "http://ec2-54-76-160-5.eu-west-1.compute.amazonaws.com/")
        super.init(baseURL: "http://0.0.0.0:8000/")
    }
    
    //Services
    var productList: Resource { return resource("api/products/") }
    var productSave: Resource { return resource("api/productsPost/") }

}
