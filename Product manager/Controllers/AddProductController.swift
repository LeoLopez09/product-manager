//
//  AddProductController.swift
//  Product manager
//
//  Created by Leonardo Florez Lopez on 10/17/18.
//  Copyright © 2018 Leonardo Florez Lopez. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import CoreLocation

class AddProductController: UIViewController, NVActivityIndicatorViewable {

    @IBOutlet var nameproduct: UITextField!
    @IBOutlet var priceProduct: UITextField!
    @IBOutlet var expirationDateProduct: UITextField!
    @IBOutlet var locationProduct: UITextField!
    
    let datePicker = UIDatePicker()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Agregar producto"
        
        //Cargador indicador
        let activityIndicatorView = NVActivityIndicatorView(frame: self.view.frame,
                                                            type: NVActivityIndicatorType(rawValue: 23)!)
        self.view.addSubview(activityIndicatorView)
        
        //Boton guardar producto
        let btnSave = UIBarButtonItem(title: "Guardar", style: .plain, target: self, action: #selector (AddProductController.saveProduct))
        self.navigationItem.rightBarButtonItem  = btnSave
        
        //Mostrar selector de fecha
        showDatePicker()
    }
    
    @objc func saveProduct(){
        
        if self.nameproduct.text?.count != 0 && self.priceProduct.text?.count != 0 &&
            self.expirationDateProduct.text?.count != 0 && self.locationProduct.text?.count != 0{
            
            self.startAnimating()
            
            let geoCoder = CLGeocoder()
            geoCoder.geocodeAddressString(self.locationProduct.text!) { (placemarks, error) in
                guard
                    let placemarks = placemarks,
                    let location = placemarks.first?.location
                     
                    else {
                        let alert = UIAlertController(title: "Alerta", message: "No es posible obtener las coordenadas de tu dirección.", preferredStyle: .alert)
                        
                        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                        
                        self.present(alert, animated: true)
                        
                        self.stopAnimating()
                        return
                }
                //print("Localizacion Latitud \(location.coordinate.latitude)")
                //print("Localizacion Longitud \(location.coordinate.longitude)")
                
                let params = ["name": self.nameproduct.text!,
                              "price": self.priceProduct.text!,
                              "date": self.expirationDateProduct.text!,
                              "latitud": String(location.coordinate.latitude),
                              "longitud": String(location.coordinate.longitude)] as [String : Any]
                
                //print("parametros \(params)")
                
                APIService.invalidateConfiguration()
                APIService.productSave.request(.post, json:params)
                    .onSuccess({ data in
                        //let response = data.jsonDict
                        //print("Responde al crear el producto  \(response)")
                        
                        let alertController = UIAlertController(title: "Petición exitosa", message: "El producto ha sido guardado.", preferredStyle: UIAlertController.Style.alert)
                        
                        let okAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                            
                            self.navigationController?.popViewController(animated: true)
                        }
                        alertController.addAction(okAction)
                        self.present(alertController, animated: true, completion:nil)
                        
                        self.stopAnimating()
                    })
                    .onFailure({error in
                        let response = error.jsonDict
                        print("error al crear el producto: \(response)")
                        
                        self.stopAnimating()
                    })
                    .onCompletion { _ in
                        self.stopAnimating()
                }
            }
            
        }else{
            let alert = UIAlertController(title: "Alerta", message: "Debe completar todos los campos.", preferredStyle: .alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            
            self.present(alert, animated: true)
        }
    }
    
    //Metodo encargado de mostrar el selector de la fecha de caducidad del producto
    func showDatePicker(){
        //Formate Date
        datePicker.datePickerMode = .date
        let loc = Locale(identifier: "es-ES")
        self.datePicker.locale = loc
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let cancelButton = UIBarButtonItem(title: "Cancelar", style: .plain, target: self, action: #selector(cancelDatePicker));
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: "OK", style: .plain, target: self, action: #selector(donedatePicker));
        
        toolbar.setItems([cancelButton,spaceButton,doneButton], animated: false)
        
        expirationDateProduct.inputAccessoryView = toolbar
        expirationDateProduct.inputView = datePicker
    }
    
    //Metodo encargado de setear la fecha en el textfield
    @objc func donedatePicker(){
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        expirationDateProduct.text = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
    
    //Metodo encargado de cerrar el toolbar sin seleccionar ninguna fecha
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
}
