//
//  ProductsController.swift
//  Product manager
//
//  Created by Leonardo Florez Lopez on 10/17/18.
//  Copyright © 2018 Leonardo Florez Lopez. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class ProductsController: UIViewController, NVActivityIndicatorViewable, UICollectionViewDataSource, UICollectionViewDelegate {

    @IBOutlet var collection: UICollectionView!
    var products = [Product]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "Productos"
     
     NotificationCenter.default.addObserver (self, selector: #selector(addProduct(_:)), name: NSNotification.Name(rawValue: "addProduct"), object: nil)
     
        //Cargador indicador
        let activityIndicatorView = NVActivityIndicatorView(frame: self.view.frame,
                                                            type: NVActivityIndicatorType(rawValue: 23)!)
        self.view.addSubview(activityIndicatorView)
    }
    
    //Cada que la vista se muestra
    override func viewDidAppear(_ animated: Bool) {
        //Metodo encargado de obtener los prodcutos
        //self.getProducts()
        //self.products.removeAll()
     
     }
     
     @objc func addProduct(_ notification: NSNotification){
          
          if let dict = notification.userInfo as NSDictionary? {
               if let name = dict["nameProduct"] as? String{
                    let newProduct = Product(idProduct: 0, nameProduct: name, imageProduct: "", expirationDateProduct: "", createdDateProduct: "", priceProduct: "", latitud: "4.809733", longitud: "-75.712903")
                    
                    self.products.append(newProduct)
                    self.collection.reloadData()
               }
          }
     }
    
    //Metodo encargado de obtener los prodcutos
    func getProducts(){
        self.startAnimating()
        APIService.invalidateConfiguration()
        APIService.productList.load()
            .onSuccess { data in
                let json = data.jsonDict
                //print("Datos Productos ",json)
                
                if let details = json["details"] as? NSArray{
                    
                    for item in details{
                        var id = 0
                        var name = ""
                        var image = ""
                        var expirationDate = ""
                        var createdDate = ""
                        var price = 0
                        var latitud = ""
                        var longitud = ""
                        
                        let product = item as! [String:Any]
                        
                        if let id_ = product["id"] as? Int{
                            id = id_
                        }
                        if let name_ = product["name"] as? String{
                            name = name_
                        }
                        if let image_ = product["image"] as? String{
                            image = image_
                        }
                        if let expirationDate_ = product["date"] as? String{
                            expirationDate = expirationDate_
                        }
                        if let createdDate_ = product["created"] as? String{
                            createdDate = createdDate_
                        }
                        if let price_ = product["price"] as? Int{
                            price = price_
                        }
                        if let latitud_ = product["latitud"] as? String{
                            latitud = latitud_
                        }
                        if let longitud_ = product["longitud"] as? String{
                           longitud = longitud_
                        }
                        
                        let newProduct = Product(idProduct: id, nameProduct: name, imageProduct: image, expirationDateProduct: expirationDate, createdDateProduct: createdDate, priceProduct: "$ \(String(price))", latitud: latitud, longitud: longitud)
                        
                        self.products.append(newProduct)
                    }
                }
            
                self.collection.reloadData()
                self.stopAnimating()
            }
            .onFailure { error in
                print("error al obtener los productos \(error)")
                self.stopAnimating()
        }
    }
    
    // MARK: - UICollectionViewDataSource protocol
    
    // Decirle a la vista de colección cuántas celdas hacer
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.products.count
    }
    
    // Hacer una celda para cada ruta de índice de celda
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // Obtener una referencia a nuestra celda de guión gráfico
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath as IndexPath) as! ProductCollectionViewCell
        
        cell.nameproduct.text = self.products[indexPath.row].nameProduct
        cell.priceProduct.text = self.products[indexPath.row].priceProduct
        
        return cell
    }
    
    // MARK: - UICollectionViewDelegate protocol
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // Manejar eventos de tap de cada celda
        
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "DetailProductController") as! DetailProductController
        nextViewController.product = self.products[indexPath.row]
        self.navigationController!.pushViewController(nextViewController, animated: true)
    }
}
