//
//  Product.swift
//  Product manager
//
//  Created by Leonardo Florez Lopez on 10/17/18.
//  Copyright © 2018 Leonardo Florez Lopez. All rights reserved.
//

import Foundation

class Product {
    
    var idProduct: Int!
    var nameProduct: String!
    var imageProduct: String!
    var expirationDateProduct: String!
    var createdDateProduct: String!
    var priceProduct: String!
    var latitud: String!
    var longitud: String!

    init(idProduct:Int, nameProduct:String, imageProduct:String, expirationDateProduct: String, createdDateProduct: String, priceProduct:String, latitud:String, longitud:String){
        
        self.idProduct = idProduct
        self.nameProduct = nameProduct
        self.imageProduct = imageProduct
        self.expirationDateProduct = expirationDateProduct
        self.createdDateProduct = createdDateProduct
        self.priceProduct = priceProduct
        self.latitud = latitud
        self.longitud = longitud
    }
}
