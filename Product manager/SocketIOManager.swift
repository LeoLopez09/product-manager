//
//  SocketIOManager.swift
//  Product manager
//
//  Created by Leonardo Florez Lopez on 11/1/18.
//  Copyright © 2018 Leonardo Florez Lopez. All rights reserved.
//

import Foundation
import SocketIO

class SocketIOManager: NSObject {
    
    static let manager = SocketManager(socketURL: URL(string: "http://192.168.0.102:3000")!, config: [.log(true), .compress])
    static let socket = manager.defaultSocket
    
    class func connectSocket(){
        socket.connect()
    }
    
    class func reciveMessage(){
        socket.on("micanal") { (data, ack) in
            for item in data{
                if let product = item as? [String:Any]{
                    if let name = product["nombreProducto"] as? String{
                        let dict:[String: String] = ["nameProduct": name]
                        NotificationCenter.default.post (name: NSNotification.Name(rawValue: "addProduct"), object: nil, userInfo: dict )
                    }
                }
            }
        }
    }
}
